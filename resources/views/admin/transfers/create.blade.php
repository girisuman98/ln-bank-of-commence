@extends('admin.layout.mainlayout')
@section('pageTitle', 'LN Bank :: Create Transfers')

@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<div class="page-wrapper mdc-toolbar-fixed-adjust">
  <main class="content-wrapper">
	<div class="mdc-layout-grid">
          <div class="mdc-layout-grid__inner">
            <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
              <div class="mdc-card">
                <section class="mdc-card__primary">
                  <h1 class="mdc-card__title mdc-card__title--large text-center page_title">Transfer Amount  <a href="/transfers" class="float-right mdc-button mdc-button--stroked">View </a></h1>
                </section>
              </div>
            </div>
            
          	<div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
					<div class="mdc-card">
						<div class="template-demo">
						  <div class="card-body px-lg-5 pt-0">
					
							<!-- Form -->
							<form class="text-center pt-5" action="/transfers/store" method="post" id="user_registration" enctype="multipart/form-data">
								@csrf
								
							<div class="form-row mb-4">
								<div class="col">
									<input type="number" maxlength="10" id="amount" name="amount" class="form-control" placeholder="Amount" required value="{{ old('amount') }}">
									@if ($errors->has('amount'))
										<div class="error">{{ $errors->first('amount') }}</div>
									@endif
								</div>
								@php $users = \App\User::all(); @endphp
								<div class="col">
									<select name="from_id" class="browser-default custom-select" required>
										<option value="">Select From User Account</option>
										@if(count($users) > 0)
											@foreach($users as $user)
												<option value="{{ $user->id }}">{{ $user->name }}({{ $user->accountno }})</option>
											@endforeach
										@endif
									</select>
									@if ($errors->has('from_id'))
										<div class="error">{{ $errors->first('from_id') }}</div>
									@endif
								</div>
							</div>
							
							<div class="form-row mb-4">
							<div class="col">
									<select name="user_id" class="browser-default custom-select" required>
										<option value="">Select To User Account</option>
										@if(count($users) > 0)
											@foreach($users as $user)
												<option value="{{ $user->id }}">{{ $user->name }}({{ $user->accountno }})</option>
											@endforeach
										@endif
									</select>
									@if ($errors->has('user_id'))
										<div class="error">{{ $errors->first('user_id') }}</div>
									@endif
								</div>
								<div class="col">
								<textarea id="details" name="details" class="form-control" placeholder="Details" required>{{ old('details') }}</textarea>
								@if ($errors->has('details'))
									<div class="error">{{ $errors->first('details') }}</div>
								@endif
								</div>
							</div>
					
								<button class="btn blue-gradient btn-rounded btn-md mb-4 waves-effect z-depth-0" type="submit">Create</button>
							</form>
							<!-- Form -->
					
						</div>
						</div>
					</div>
				</div>
          </div>
        </div>
  </main>
<style>
input,select {
    border-radius:0 !important;
}
.event_date{
	visibility:hidden;
}
.error{
	font-size: 14px;
    text-align: left;
    color: red;
	margin-bottom: 0;
}
.display_none{
	display:none;
}
select{
	font-size: 13px;
    color: #495057 !important;
    opacity: 0.9;
}
.font-14{
	font-size:14px;
}
.nicEdit-main{
	text-align:left;
}
</style>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
@if (Session::has('success'))
  swal("Thank You!"," {{ Session::get('success') }}", "success");
@endif

@if (Session::has('error'))
  swal("Error!"," {{ Session::get('error') }}", "error");
@endif
</script>
@endsection
