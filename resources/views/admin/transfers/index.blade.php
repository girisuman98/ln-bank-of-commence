@extends('admin.layout.mainlayout')
@section('pageTitle', 'LN Bank :: User Transfers')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<div class="page-wrapper mdc-toolbar-fixed-adjust">
  <main class="content-wrapper">
	<div class="mdc-layout-grid">
          <div class="mdc-layout-grid__inner">
            <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
              <div class="mdc-card">
                <section class="mdc-card__primary">
                  <h1 class="mdc-card__title mdc-card__title--large text-center page_title">List of Transfers ({{ count($data) }}) <a href="/transfers/create" class="float-right mdc-button mdc-button--stroked">Add +</a></h1>
                </section>
              </div>
            </div>
            
          	<div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
					<div class="mdc-card">
						<div class="template-demo p-3">
						  @if(count($data) > 0)
						  <table class="table table-responsive-mobile table-bordered" id="users_list">
						  <thead>
							<tr>
							  <th>Name</th>
							  <th>Email Id</th>
							  <th>Mobile No</th>
							  <th>A/C No</th>
							  <th>Amount</th>
							  <th>Details</th>
							  <th>Type</th>
							</tr>
						  </thead>
						  <tbody>
						  	@foreach($data as $item)
							<tr>
							  <td>{{ $item->user->name }} </td>
							  <td>{{ $item->user->email }}</td>
							  <td>{{ $item->user->mobile }}</td>
							  <td>{{ $item->user->accountno }}</td>
							 <td>{{ $item->amount }}</td>
							  <td>{{ $item->details }}</td>
							  <td>
							  	@if($item->type == "1")
									<span class="badge badge-success">Deposited</span>
								@else
									<span class="badge badge-default">Transferred</span>
								@endif
							  </td>
							</tr>
							@endforeach
							</tbody>
						  </table>
						  @else
						  	<p class="m-0">Sorry, no transfer found</p>
						  @endif
						</div>
					</div>
				</div>
          </div>
        </div>
		
  </main>
@endsection
@section('extraFooter')
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script>
	 $('#users_list').DataTable({
					responsive: true,
					"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
					aaSorting: []
				});
	$('.dataTables_length').addClass('bs-select');
	
 @if (Session::has('success'))
  swal("Thank You!"," {{ Session::get('success') }}", "success");
@endif

@if (Session::has('error'))
  swal("Error!"," {{ Session::get('error') }}", "error");
@endif
</script>
<style>
.display_none{
	display:none;
}
select{
	display: inline !important;
}
.table tbody tr td:last-child {
    padding-left: 0;
}
.table tbody tr td {
    width:auto !important;
}
.table thead tr th {
    width:auto !important;
}
.table thead tr th:first-child {
     padding-left: 0px; 
}
#pilots_list a {
    margin-right: 0.8rem!important;
}
</style>
@endsection


