<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('pageTitle')</title>
	@include('admin.layout.partials.head')
</head>
<body>
	@include('admin.layout.partials.nav')
	
	@yield('content')
	
	@include('admin.layout.partials.footer')
	
	@yield('extraFooter')
</body>
</html>