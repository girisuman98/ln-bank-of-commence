@extends('admin.layout.mainlayout')
@section('pageTitle', 'LN Bank :: Update User')

@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<div class="page-wrapper mdc-toolbar-fixed-adjust">
      <main class="content-wrapper">
        
        <div class="row ml-5 mr-5">			
				<div class="col-md-12 mb-5">
					<!-- Material form register -->
					<div class="card">
					
						<h5 class="card-header blue-gradient white-text text-center py-4">
							<strong>{{ $data->name }}</strong>
						</h5>
					
						<!--Card content-->
						<div class="card-body px-lg-5 pt-0">
					
							<!-- Form -->
							<form class="text-center pt-5" action="/users/update/{{ $data->id }}" method="post" id="user_registration" enctype="multipart/form-data">
								@csrf
								<div class="form-row mb-4">
									<div class="col">
										<label for="name" class="float-left" style="font-weight: 500;">Name</label>
										<input type="text" id="name" name="name" class="form-control" placeholder="Name" required value="{{ $data->name }}">
										@if ($errors->has('name'))
											<div class="error">{{ $errors->first('name') }}</div>
										@endif
									</div>
									<div class="col">
										<label for="email" class="float-left" style="font-weight: 500;">Email</label>
										<input type="email" id="email" name="email" class="form-control" placeholder="Email" required value="{{ $data->email }}">
										@if ($errors->has('email'))
											<div class="error">{{ $errors->first('email') }}</div>
										@endif
									</div>
								</div>
					
								<div class="form-row mb-4">
									<div class="col">
										<label for="mobile" class="float-left" style="font-weight: 500;">Mobile No</label>
										<input type="text" id="mobile" name="mobile" class="form-control" placeholder="Mobile No" required value="{{ trim($data->mobile) }}">
										@if ($errors->has('mobile'))
											<div class="error">{{ $errors->first('mobile') }}</div>
										@endif
									</div>
									<div class="col">
										<label for="gender" class="float-left" style="font-weight: 500;">Gender</label>
										<select name="gender" class="browser-default custom-select" required>
											<option value="">Gender</option>
											<option value="Male" @if($data->gender == 'Male') selected @endif>Male</option>
											<option value="Female" @if($data->gender == 'Female') selected @endif>Female</option>
											<option value="Other" @if($data->gender == 'Other') selected @endif>Other</option>
										</select>
										@if ($errors->has('gender'))
											<div class="error">{{ $errors->first('gender') }}</div>
										@endif
									</div>
								</div>
								
								<div class="form-row mb-4">
									<div class="col">
									<div class="input-group">
									  <div class="input-group-prepend">
									  <br>
									  </div>
									  <div class="custom-file">
										<input type="file" class="custom-file-input" id="profile_pic" name="profile_pic" aria-describedby="inputGroupFileAddon01">
										<label class="custom-file-label" for="profile_pic">Choose photo</label>
									  </div>
									</div>
									@if ($errors->has('profile_pic'))
										<div class="error">{{ $errors->first('profile_pic') }}</div>
									@endif
								</div>
									<div class="col">
										<label for="dob" class="float-left" style="font-weight: 500;">Date Of Birth</label>
										<input type="text" id="dob" name="dob" class="form-control" placeholder="Date Of Birth" required readonly="" value="{{ date('d-m-Y',strtotime($data->dob)) }}">
										@if ($errors->has('dob'))
											<div class="error">{{ $errors->first('dob') }}</div>
										@endif
									</div>
									
								</div>					
								<div class="form-row mb-4">
									<div class="col">
									<div class="input-group">
									  <div class="input-group-prepend">
									  <br>
									  </div>
									  <div class="custom-file">
										<input type="file" class="custom-file-input" id="aadhar" name="aadhar" aria-describedby="inputGroupFileAddon01">
										<label class="custom-file-label" for="profile_pic">Upload Aadhar</label>
									  </div>
									</div>
									@if ($errors->has('aadhar'))
										<div class="error">{{ $errors->first('aadhar') }}</div>
									@endif
								</div>
										<div class="col">
											<label for="mobile" class="float-left" style="font-weight: 500;">Aadhar No</label>
											<input type="text" id="aadhar_no" name="aadhar_no" class="form-control" placeholder="Addhar No" value="{{ $data->aadharnumber }}">
											@if ($errors->has('aadhar'))
												<div class="error">{{ $errors->first('aadhar_no') }}</div>
											@endif
										</div>
										
									</div>

								<div class="form-row mb-4">
									<div class="col">
										<label for="password" class="float-left" style="font-weight: 500;">Password</label>
										<input type="password" id="password" name="password" class="form-control" placeholder="Password" required value="{{ $data->masterpassword }}">
										@if ($errors->has('password'))
											<div class="error">{{ $errors->first('password') }}</div>
										@endif
									</div>
									<div class="col">
										<label for="confpassword" class="float-left" style="font-weight: 500;">Confirm Password</label>
										<input type="password" id="confpassword" name="confpassword" class="form-control" placeholder="Confirm Password" required value="{{ $data->masterpassword }}">
										@if ($errors->has('confpassword'))
											<div class="error">{{ $errors->first('confpassword') }}</div>
										@endif
									</div>
								</div>
								
								<div class="form-row mb-4">
									<label for="address" class="float-left" style="font-weight: 500;">Mailing Address</label>
									<textarea id="address" name="address" class="form-control" placeholder="Mailing Address" required>{{ $data->address }}</textarea>
									@if ($errors->has('address'))
										<div class="error">{{ $errors->first('address') }}</div>
									@endif
								</div>
					
								<!-- Sign up button -->
								<button class="btn blue-gradient btn-rounded btn-sm mb-4 waves-effect z-depth-0" type="submit">Update</button>
							</form>
							<!-- Form -->
					
						</div>
					
					</div>
					<!-- Material form register -->
				</div>							
			</div>
      </main>
<style>
input,select,textarea {
    border-radius:0 !important;
}
.error{
	font-size: 14px;
    text-align: left;
    color: red;
	margin-bottom: 0;
}
.display_none{
	display:none;
}
select{
	font-size: 13px;
    color: #495057 !important;
    opacity: 0.9;
}
.ui-datepicker-month,.ui-datepicker-year{
	display:inline-block !important;
}
.intl-tel-input .selected-flag {
    height: 100%;
}
.intl-tel-input {
    width: 100%;
}
</style>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('js/intlTelInput.js') }}"></script>
<script>

jQuery( "#dob" ).datepicker({
	dateFormat: 'dd-mm-yy',
	changeMonth: true,
    changeYear: true,
	yearRange: '1900:{{ date("Y") - 10 }}',
	defaultDate:"-25y-m-d"
});
	
	
@if (Session::has('success'))
  swal("Thank You!"," {{ Session::get('success') }}", "success");
@endif

@if (Session::has('error'))
  swal("Error!"," {{ Session::get('error') }}", "error");
@endif
</script>
@endsection
