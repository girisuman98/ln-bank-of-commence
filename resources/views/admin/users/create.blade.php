@extends('admin.layout.mainlayout')
@section('pageTitle', 'LN Bank :: Create User')

@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<div class="page-wrapper mdc-toolbar-fixed-adjust">
  <main class="content-wrapper">
	<div class="mdc-layout-grid">
          <div class="mdc-layout-grid__inner">
            <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
              <div class="mdc-card">
                <section class="mdc-card__primary">
                  <h1 class="mdc-card__title mdc-card__title--large text-center page_title">Create New User  <a href="/users" class="float-right mdc-button mdc-button--stroked">View + </a></h1>
                </section>
              </div>
            </div>
            
          	<div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
					<div class="mdc-card">
						<div class="template-demo">
						  <div class="card-body px-lg-5 pt-0">
					
							<!-- Form -->
							<form class="text-center pt-5" action="/users/store" method="post" id="user_registration" enctype="multipart/form-data">
								@csrf
								<div class="form-row mb-4">
								<div class="col">
									<input type="text" id="name" name="name" class="form-control" placeholder="Name" required value="{{ old('name') }}">
									@if ($errors->has('name'))
										<div class="error">{{ $errors->first('name') }}</div>
									@endif
								</div>
								<div class="col">
									<input type="email" id="email" name="email" class="form-control" placeholder="Email" required value="{{ old('email') }}">
									@if ($errors->has('email'))
										<div class="error">{{ $errors->first('email') }}</div>
									@endif
								</div>
							</div>
							
							<div class="form-row mb-4">
								<div class="col">
									<input type="number" maxlength="10" id="mobile" name="mobile" class="form-control" placeholder="Mobile No" required value="{{ old('mobile') }}">
									@if ($errors->has('mobile'))
										<div class="error">{{ $errors->first('mobile') }}</div>
									@endif
								</div>
								<div class="col">
									<select name="gender" class="browser-default custom-select" required>
										<option value="">Gender</option>
										<option value="Male" @if(old('gender') == 'Male') selected @endif>Male</option>
										<option value="Female" @if(old('gender') == 'Female') selected @endif>Female</option>
										<option value="Other" @if(old('gender') == 'Other') selected @endif>Other</option>
									</select>
									@if ($errors->has('gender'))
										<div class="error">{{ $errors->first('gender') }}</div>
									@endif
								</div>
							</div>
							
							<div class="form-row mb-4">
								<div class="col">
									<div class="input-group">
									  <div class="input-group-prepend">
									  </div>
									  <div class="custom-file">
										<input type="file" class="custom-file-input" id="profile_pic" name="profile_pic" aria-describedby="inputGroupFileAddon01">
										<label class="custom-file-label" for="profile_pic">Choose photo</label>
									  </div>
									</div>
									@if ($errors->has('profile_pic'))
										<div class="error">{{ $errors->first('profile_pic') }}</div>
									@endif
								</div>
								<div class="col">
									<input type="text" id="dob" name="dob" class="form-control" placeholder="Date Of Birth" required readonly="" value="{{ old('dob') }}">
									@if ($errors->has('dob'))
										<div class="error">{{ $errors->first('dob') }}</div>
									@endif
								</div>
							</div>
							
							<div class="form-row mb-4">
								<div class="col">
									<div class="input-group">
									  <div class="input-group-prepend">
									  </div>
									  <div class="custom-file">
										<input type="file" class="custom-file-input" id="aadhar" name="aadhar" aria-describedby="inputGroupFileAddon01">
										<label class="custom-file-label" for="aadhar">Upload Aadhar</label>
									  </div>
									</div>
									@if ($errors->has('aadhar'))
										<div class="error">{{ $errors->first('aadhar') }}</div>
									@endif
								</div>
								<div class="col">
									<input type="text" id="aadhar_no" name="aadhar_no" class="form-control" placeholder="Aadhar No" required="" value="{{ old('aadhar_no') }}">
									@if ($errors->has('aadhar_no'))
										<div class="error">{{ $errors->first('aadhar_no') }}</div>
									@endif
								</div>
							</div>
							
							<div class="form-row mb-4">
								<div class="col">
									<input type="password" id="password" name="password" class="form-control" placeholder="Password" required value="{{ old('password') }}">
									@if ($errors->has('password'))
										<div class="error">{{ $errors->first('password') }}</div>
									@endif
								</div>
								<div class="col">
									<input type="password" id="confpassword" name="confpassword" class="form-control" placeholder="Confirm Password" required value="{{ old('confpassword') }}">
									@if ($errors->has('confpassword'))
										<div class="error">{{ $errors->first('confpassword') }}</div>
									@endif
								</div>
							</div>
							
							<div class="form-row mb-4">
								<textarea id="address" name="address" class="form-control" placeholder="Mailing Address" required>{{ old('address') }}</textarea>
								@if ($errors->has('address'))
									<div class="error">{{ $errors->first('address') }}</div>
								@endif
							</div>
					
								<button class="btn blue-gradient btn-rounded btn-md mb-4 waves-effect z-depth-0" type="submit">Create</button>
							</form>
							<!-- Form -->
					
						</div>
						</div>
					</div>
				</div>
          </div>
        </div>
  </main>
<style>
input,select {
    border-radius:0 !important;
}
.event_date{
	visibility:hidden;
}
.error{
	font-size: 14px;
    text-align: left;
    color: red;
	margin-bottom: 0;
}
.display_none{
	display:none;
}
select{
	font-size: 13px;
    color: #495057 !important;
    opacity: 0.9;
}
.font-14{
	font-size:14px;
}
.nicEdit-main{
	text-align:left;
}
</style>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
jQuery( "#dob" ).datepicker({
	dateFormat: 'dd-mm-yy',
	changeMonth: true,
    changeYear: true,
	yearRange: '1900:2009',
	defaultDate:"-25y-m-d"
});
@if (Session::has('success'))
  swal("Thank You!"," {{ Session::get('success') }}", "success");
@endif

@if (Session::has('error'))
  swal("Error!"," {{ Session::get('error') }}", "error");
@endif
</script>
@endsection
