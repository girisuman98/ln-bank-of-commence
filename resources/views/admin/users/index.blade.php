@extends('admin.layout.mainlayout')
@section('pageTitle', 'LN Bank :: Users')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<div class="page-wrapper mdc-toolbar-fixed-adjust">
  <main class="content-wrapper">
	<div class="mdc-layout-grid">
          <div class="mdc-layout-grid__inner">
            <div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
              <div class="mdc-card">
                <section class="mdc-card__primary">
                  <h1 class="mdc-card__title mdc-card__title--large text-center page_title">List of Users ({{ count($data) }}) <a href="/users/create" class="float-right mdc-button mdc-button--stroked">Add +</a></h1>
                </section>
              </div>
            </div>
            
          	<div class="mdc-layout-grid__cell stretch-card mdc-layout-grid__cell--span-12">
					<div class="mdc-card">
						<div class="template-demo p-3">
						  @if(count($data) > 0)
						  <table class="table table-responsive table-bordered" id="users_list">
						  <thead>
							<tr>
							  <th>Name</th>
							  <th>Email Id</th>
							  <th>Mobile No</th>
							  <th>DOB</th>
							  <th>Gender</th>
							  <th>A/C No</th>
							  <th>Balance</th>
							  <th>Aadhar</th>
							  <th>Photo</th>
							  <th>Status</th>
							  <th>Actions</th>
							</tr>
						  </thead>
						  <tbody>
						  	@foreach($data as $item)
							<tr>
							  <td>{{ $item->name }} </td>
							  <td>{{ $item->email }}</td>
							  <td>{{ $item->mobile }}</td>
							  <td>{{ $item->dob }}</td>
							  <td>{{ $item->gender }}</td>
							  <td>{{ $item->accountno }}</td>
							  <td>{{ \App\Usertransaction::where('user_id', $item->id)->where('type', '1')->sum('amount') - \App\Usertransaction::where('user_id', $item->id)->where('type', '0')->sum('amount') }}</td>
							  <td><a href="{{ asset('uploads/useraadhars/') }}/{{ $item->aadharurl }}">{{ $item->aadharnumber }}</a></td>
							  <td><img style="width: 30px;" src="{{ asset('uploads/userpics/') }}/{{ $item->imageurl }}" /></td>
							  <td>
							  	@if($item->status == "1")
									<span class="badge badge-success">Active</span>
								@else
									<span class="badge badge-danger">Inactive</span>
								@endif
							  </td>
							  <td class="d-flex">
							  <span><a href="/users/edit/{{ $item->id }}" title="Edit">Edit</a></span>
							  @if($item->status == "1")
							  <span><a href="/users/delete/{{ $item->id }}" title="Inactive" class="delete_member">Deactivate</a></span>
							  @else
							  <span><a href="/users/active/{{ $item->id }}" title="Active" class="active_member">Activate</a></span>
							  @endif
							  </td>
							</tr>
							@endforeach
							</tbody>
						  </table>
						  @else
						  	<p class="m-0">Sorry, no user found</p>
						  @endif
						</div>
					</div>
				</div>
          </div>
        </div>
		
  </main>
@endsection
@section('extraFooter')
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script>
	 $('#users_list').DataTable({
					responsive: true,
					"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
					aaSorting: []
				});
	$('.dataTables_length').addClass('bs-select');
	
	jQuery( document ).ready(function() {
		function warnBeforereDelete(linkURL) {
		swal({
			title: "Are you sure to inactive this user?",
			type: "warning",
			showCancelButton: true,
			confirmButtonText: "Yes",
			cancelButtonText: "No",
			closeOnConfirm: false,
			closeOnCancel: true
		},
		function(isConfirm){
		  if (isConfirm) {
			window.location.href = linkURL;
		  }
		});	
	
	}
	
	jQuery(document).on('click',".delete_member", function (e) {
		e.preventDefault();
		var linkURL = jQuery(this).attr("href");
		warnBeforereDelete(linkURL);
	}); 
	
	function warnBeforereActive(linkURL) {
		swal({
			title: "Are you sure to active this user?",
			type: "warning",
			showCancelButton: true,
			confirmButtonText: "Yes",
			cancelButtonText: "No",
			closeOnConfirm: false,
			closeOnCancel: true
		},
		function(isConfirm){
		  if (isConfirm) {
			window.location.href = linkURL;
		  }
		});	
	
	}
	
	jQuery(document).on('click',".active_member", function (e) {
		e.preventDefault();
		var linkURL = jQuery(this).attr("href");
		warnBeforereActive(linkURL);
	}); 
	
	
 });
 
 @if (Session::has('success'))
  swal("Thank You!"," {{ Session::get('success') }}", "success");
@endif

@if (Session::has('error'))
  swal("Error!"," {{ Session::get('error') }}", "error");
@endif
</script>
<style>
.display_none{
	display:none;
}
select{
	display: inline !important;
}
.table tbody tr td:last-child {
    padding-left: 0;
}
.table tbody tr td {
    width:auto !important;
}
.table thead tr th {
    width:auto !important;
}
.table thead tr th:first-child {
     padding-left: 0px; 
}
#pilots_list a {
    margin-right: 0.8rem!important;
}
</style>
@endsection


