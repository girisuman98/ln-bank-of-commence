<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
			$table->date('dob')->format('Y-m-d')->nullable(); 
			$table->string('gender', '255')->nullable();
			$table->string('mobile', '30')->unique();
			$table->text('accountno')->nullable();
			$table->text('imageurl')->nullable();
			$table->string('aadharnumber', '16')->nullable();
			$table->text('aadharurl')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
			 $table->string('masterpassword');
			$table->text('address')->nullable();
			$table->enum('status', array('0', '1'))->default('1');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
