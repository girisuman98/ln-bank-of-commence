Steps to run the code:
    - Install the laravel
    - Create one database and put the credential in .env file.
    - Then run the database migration files.
    - Start the server.
That's it.
