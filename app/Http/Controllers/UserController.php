<?php

namespace App\Http\Controllers;

use Session;
use App\User;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
		$data = User::orderBy('id','DESC')->get();
        return view('admin.users.index',compact('data'));
    }
	
	public function create()
    {
        return view('admin.users.create');
    }
	
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			"name"    => "required|min:3",
			"email"    => "required|unique:users,email",
			"mobile"    => "required|unique:users,mobile",
			"gender"    => "required",
			"address"    => "required",
			"aadhar_no"    => "required",
			"dob"    => "required",
			'password' => 'min:6|required_with:confpassword|same:confpassword',
			'confpassword' => 'min:6'
		]);
		
		$name = $request->name; 
		$email = $request->email; 
		$mobile =  $request->mobile;
		$gender = $request->gender; 
		$address = $request->address; 
		$aadhar_no = $request->aadhar_no; 
		$dob = date('Y-m-d', strtotime($request->dob));
		$password =  $request->password;
		$confpassword =  $request->confpassword; 
		
		if($request->hasFile('profile_pic') == false)
		{
			Session::flash('error', 'Please upload profile photo.');
			return Redirect::back()->withErrors($validator)->withInput(Input::all());
		}
		if($request->hasFile('aadhar') == false)
		{
			Session::flash('error', 'Please upload aadhar card.');
			return Redirect::back()->withErrors($validator)->withInput(Input::all());
		}
		if($validator->fails()) 
		{
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        }
		else
		{
			$user = new User();
			
			$user->name = $name;
			$user->email = $email;
			$user->mobile = $mobile;
			$user->gender = $gender;
			$user->dob = $dob;
			$user->address = $address;
			$user->aadharnumber = $aadhar_no;
			$user->masterpassword = $password;
			$user->password = bcrypt($password);
			
			if($request->hasFile('profile_pic'))
			{
				$rules = array('file' => 'mimes:png,gif,jpeg|max:2000');
				$validator_file = Validator::make(array('file'=> Input::file('profile_pic')), $rules);
				if($validator_file->passes())
				{
					$getimageName = time().'.'.$request->profile_pic->getClientOriginalExtension();
					$request->profile_pic->move(public_path('uploads/userpics'), $getimageName);
			
					$user->imageurl = $getimageName;	
				}
			}
			
			if($request->hasFile('aadhar'))
			{
				$rules = array('file' => 'mimes:png,gif,jpeg|max:2000');
				$validator_file = Validator::make(array('file'=> Input::file('aadhar')), $rules);
				if($validator_file->passes())
				{
					$fileName = time().'.'.$request->aadhar->getClientOriginalExtension();
					$request->aadhar->move(public_path('uploads/useraadhars'), $fileName);
			
					$user->aadharurl = $fileName;	
				}
			}
			$user->accountno = $this->randomuniquecode();
			$user->save();
			
			Session::flash('success', 'Saved successfully');
			return Redirect()->back();
		}
    }
	
	public static function randomuniquecode(){
		$random = 'LNB'.rand(1111111111,mt_getrandmax());
		$count = User::where('accountno', $random)->count();
		if( $count == 0)
			return $random;
		else
			$this->randomuniquecode();
	}
	
	public function edit($id)
    {
		$data = User::find($id);
        return view('admin.users.edit',compact('data'));
    }
	
	public function update(Request $request,$id)
	{
		$validator = Validator::make($request->all(), [
			"name"    => "required|min:3",
			"email"    => "required|unique:users,email,".$id,
			"mobile"    => "required|unique:users,mobile,".$id,
			"gender"    => "required",
			"address"    => "required",
			"aadhar_no"    => "required",
			"dob"    => "required",
			'password' => 'min:6|required_with:confpassword|same:confpassword',
			'confpassword' => 'min:6'
		]);
		
		$name = $request->name; 
		$email = $request->email; 
		$mobile =  $request->mobile;
		$gender = $request->gender; 
		$address = $request->address; 
		$aadhar_no = $request->aadhar_no; 
		$dob = date('Y-m-d', strtotime($request->dob));
		$password =  $request->password;
		$confpassword =  $request->confpassword; 
		
		if($validator->fails()) 
		{
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        }
		else
		{
			$user = User::find($id);
			
			$user->name = $name;
			$user->email = $email;
			$user->mobile = $mobile;
			$user->gender = $gender;
			$user->dob = $dob;
			$user->address = $address;
			$user->aadharnumber = $aadhar_no;
			$user->masterpassword = $password;
			$user->password = bcrypt($password);
			
			if($request->hasFile('profile_pic'))
			{
				$rules = array('file' => 'mimes:png,gif,jpeg|max:2000');
				$validator_file = Validator::make(array('file'=> Input::file('profile_pic')), $rules);
				if($validator_file->passes())
				{
					$getimageName = time().'.'.$request->profile_pic->getClientOriginalExtension();
					$request->profile_pic->move(public_path('uploads/userpics'), $getimageName);
			
					$user->imageurl = $getimageName;	
				}
			}
			
			if($request->hasFile('aadhar'))
			{
				$rules = array('file' => 'mimes:png,gif,jpeg|max:2000');
				$validator_file = Validator::make(array('file'=> Input::file('aadhar')), $rules);
				if($validator_file->passes())
				{
					$fileName = time().'.'.$request->aadhar->getClientOriginalExtension();
					$request->aadhar->move(public_path('uploads/useraadhars'), $fileName);
			
					$user->aadharurl = $fileName;	
				}
			}
			$user->save();
			
			Session::flash('success', 'Updated successfully');
			return Redirect()->back();
		}
    }
	
	public function deactivate($id)
    {
		$user = User::find($id); 
		if($user == NULL)
		{
			Session::flash('error', 'Somthing went wrong');
			return Redirect::back();
		}
		
		$user->status = '0';
		$user->save();
		Session::flash('success', 'User account deactivated successfully');
		return Redirect::back();	
	}
	
	public function activate($id)
    {
		$user = User::find($id); 
		if($user == NULL)
		{
			Session::flash('error', 'Somthing went wrong');
			return Redirect::back();
		}
		
		$user->status = '1';
		$user->save();
		Session::flash('success', 'User account deactivated successfully');
		return Redirect::back();	
	}
	
}
