<?php

namespace App\Http\Controllers;

use Session;
use App\User;
use Validator;
use App\Usertransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class TransactionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
		$data = Usertransaction::with('user')->where('method','Cash')->orderBy('id','DESC')->get();
        return view('admin.transactions.index',compact('data'));
    }
	
	public function create()
    {
        return view('admin.transactions.create');
    }
	
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			"amount"    => "required",
			"user_id"    => "required",
			"type"    => "required",
			"details"    => "required"
		]);
		
		$amount = $request->amount; 
		$user_id = $request->user_id; 
		$type =  $request->type;
		$details = $request->details; 
		
		if($type == '0')
		{
			$balance = Usertransaction::where('user_id', $user_id)->where('type', '1')->sum('amount');
			$credits = Usertransaction::where('user_id', $user_id)->where('type', '0')->sum('amount');
			if(($amount + $credits) > $balance)
			{
				Session::flash('error', 'Low Balance.');
				return Redirect::back()->withErrors($validator)->withInput(Input::all());
			}
		}
		
		if($validator->fails()) 
		{
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        }
		else
		{
			$usertransaction = new Usertransaction();
			
			$usertransaction->amount = $amount;
			$usertransaction->user_id = $user_id;
			$usertransaction->type = $type;
			$usertransaction->details = $details;
			$usertransaction->save();
			
			Session::flash('success', 'Saved successfully');
			return Redirect()->back();
		}
    }
	
}
