<?php

namespace App\Http\Controllers;

use Session;
use App\User;
use Validator;
use App\Usertransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class TransferController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
		$data = Usertransaction::with('user')->where('method','Transfer')->orderBy('id','DESC')->get();
        return view('admin.transfers.index',compact('data'));
    }
	
	public function create()
    {
        return view('admin.transfers.create');
    }
	
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			"amount"    => "required",
			"user_id"    => "required",
			"from_id"    => "required",
			"details"    => "required"
		]);
		
		$amount = $request->amount; 
		$user_id = $request->user_id; 
		$from_id = $request->from_id; 
		$details = $request->details; 
		if($user_id == $from_id)
		{
			Session::flash('error', 'From a/c and To a/c can not be same.');
			return Redirect::back()->withErrors($validator)->withInput(Input::all());
		}
		
		$balance = Usertransaction::where('user_id', $from_id)->where('type', '1')->sum('amount');
		$credits = Usertransaction::where('user_id', $from_id)->where('type', '0')->sum('amount');
		if(($amount + $credits) > $balance)
		{
			Session::flash('error', 'Low Balance in From user a/c.');
			return Redirect::back()->withErrors($validator)->withInput(Input::all());
		}
		
		if($validator->fails()) 
		{
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        }
		else
		{
			$usertransaction = new Usertransaction();
			
			$usertransaction->amount = $amount;
			$usertransaction->user_id = $user_id;
			$usertransaction->type = '1';
			$usertransaction->method = 'Transfer';
			$usertransaction->details = $details;
			$usertransaction->save();
			
			$usertransaction = new Usertransaction();
			
			$usertransaction->amount = $amount;
			$usertransaction->user_id = $from_id;
			$usertransaction->type = '0';
			$usertransaction->method = 'Transfer';
			$usertransaction->details = $details;
			$usertransaction->save();
			
			Session::flash('success', 'Transferred successfully');
			return Redirect()->back();
		}
    }
	
}
