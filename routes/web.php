<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/users', 'UserController@index')->name('users');
Route::get('/users/create', 'UserController@create')->name('userscreate');
Route::post('/users/store', 'UserController@store');
Route::get('/users/edit/{id}', 'UserController@edit');
Route::post('/users/update/{id}', 'UserController@update');
Route::get('/users/delete/{id}', 'UserController@deactivate');
Route::get('/users/active/{id}', 'UserController@activate');

Route::get('/transactions', 'TransactionController@index');
Route::get('/transactions/create', 'TransactionController@create');
Route::post('/transactions/store', 'TransactionController@store');

Route::get('/transfers', 'TransferController@index');
Route::get('/transfers/create', 'TransferController@create');
Route::post('/transfers/store', 'TransferController@store');

Route::get('/cheque-deposits', 'ChequeController@index');
Route::get('/cheque-deposits/create', 'ChequeController@create');
Route::post('/cheque-deposits/store', 'ChequeController@store');

